#ifndef SELECT_STATE_H_
#define SELECT_STATE_H_

#include "WindowState.h"
#include "Container.h"

namespace states{

	class SelectState :	public WindowState
	{
		int start;
		void updateBtns();
		bool updateRequired;

		enum{ lvlsPerPage = 12 };
	public:
		SelectState(StateStack& stack, Context& context);
		virtual bool update(sf::Time deltaTime) override;
		~SelectState();
	};

}
#endif