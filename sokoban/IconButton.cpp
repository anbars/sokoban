#include "IconButton.h"

namespace GUI{

	IconButton::IconButton(const sf::Texture& btnTexture, const SoundHolder& sounds, sf::Keyboard::Key key) :CommonButton(btnTexture, sounds.get(audio::Sounds::Click))
	{
		hotKey = key;
	}


	IconButton::~IconButton()
	{
	}

	void IconButton::select()
	{
		sprite.setColor(sf::Color::Green);
		CommonButton::select();
	}

	void IconButton::deselect()
	{
		sprite.setColor(sf::Color::White);
		CommonButton::deselect();
	}


	void IconButton::handleEvent(const sf::Event& event, const sf::RenderWindow& window)
	{
		if (event.type == sf::Event::KeyReleased)
		{
			if (event.key.code == hotKey)
			{
				activate();
			}
		}
		else
		{
			CommonButton::handleEvent(event, window);
		}
	}

}
