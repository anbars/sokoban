#include "Button.h"

namespace GUI{

	Button::Button(TextureHolder& textures, SoundHolder& sounds, FontHolder& fonts, const std::string& label, sf::Keyboard::Key key) : textures(textures), CommonButton(textures.get(tex::Textures::stdBtn), sounds.get(audio::Sounds::Click)), text(label, fonts.get(font::Fonts::LabelFont))
	{
		hotKey = key;
		tex::centerOrigin(text);
	}


	Button::~Button()
	{
	}



	void Button::select()
	{
		sprite.setTexture(textures.get(tex::Textures::stdBtnOn));
		CommonButton::select();
	}

	void Button::deselect()
	{
		sprite.setTexture(textures.get(tex::Textures::stdBtn));
		CommonButton::deselect();
	}



	void Button::draw(sf::RenderTarget& target, sf::RenderStates states) const
	{
		states.transform *= getTransform();
		target.draw(sprite, states);
		target.draw(text, states);
	}

	void Button::handleEvent(const sf::Event& event, const sf::RenderWindow& window)
	{
		if (event.type == sf::Event::KeyReleased)
		{
			if (event.key.code == hotKey)
			{
				activate();
			}
		}
		else
		{
			CommonButton::handleEvent(event, window);
		}
	}

}
