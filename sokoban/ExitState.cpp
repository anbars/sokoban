#include "ExitState.h"
#include "Label.h"
#include "Button.h"

namespace states{

	ExitState::ExitState(StateStack& stack, Context& context) : State(stack, context), backSprite(context.textures.get(tex::Textures::dialogBlack))
	{
		using namespace GUI;
		tex::centerOrigin(backSprite);
		backSprite.setPosition(context.window.getView().getSize() / 2.f);

		shadowRect.setSize(context.window.getView().getSize());
		shadowRect.setFillColor(sf::Color(0, 0, 0, 150));

		auto label = new Label("Do you realy want to exit?", getContext().fonts);
		label->setPosition(backSprite.getPosition().x, backSprite.getPosition().y - backSprite.getLocalBounds().height / 2 + label->getLocalBounds().height / 2 + 40.f);
		components.pack(Component::Ptr(label));

		auto button = new Button(getContext().textures, getContext().sounds, getContext().fonts, "Cancel");
		button->setAction([this](){
			requestStackPop();
		});
		button->setPosition(backSprite.getPosition().x + backSprite.getLocalBounds().width/2 - button->getLocalBounds().width / 2-40.f, backSprite.getPosition().y +backSprite.getLocalBounds().height/2 - button->getLocalBounds().height / 2 - 40.f);
		components.pack(Component::Ptr(button));

		button = new Button(getContext().textures, getContext().sounds, getContext().fonts, "Confirm");
		button->setAction([this](){
			requestStackClear();
		});
		button->setPosition(backSprite.getPosition().x - backSprite.getLocalBounds().width / 2 + button->getLocalBounds().width / 2+40.f, backSprite.getPosition().y + backSprite.getLocalBounds().height / 2 - button->getLocalBounds().height / 2 - 40.f);
		components.pack(Component::Ptr(button));
	}
	

	ExitState::~ExitState()
	{
	}

	void ExitState::draw()
	{
		getContext().window.draw(shadowRect);
		getContext().window.draw(backSprite);
		getContext().window.draw(components);
	}

	bool ExitState::update(sf::Time deltaTime)
	{
		return false;
	}

	bool ExitState::handleEvent(const sf::Event& event)
	{
		if (event.type == sf::Event::KeyReleased&&event.key.code == sf::Keyboard::Escape)
		{
			requestStackPop();
		}
		else
		{
			components.handleEvent(event, getContext().window);
		}

		return false;
	}

}