#ifndef BUTTON_H_
#define BUTTON_H_
#include "CommonButton.h"
#include "ResourceHolder.h"
#include <functional>

namespace GUI{
	class Button : public CommonButton
	{
		TextureHolder& textures;
		
		sf::Text text;

		sf::Keyboard::Key hotKey;

	public:
		Button(TextureHolder& textures, SoundHolder& sounds, FontHolder& fonts, const std::string& text, sf::Keyboard::Key = sf::Keyboard::KeyCount);
		~Button();
		
		virtual void select() override;
		virtual void deselect() override;

		virtual void draw(sf::RenderTarget& target, sf::RenderStates states) const override;
		virtual void handleEvent(const sf::Event& event, const sf::RenderWindow& window) override;

	};


}
#endif

