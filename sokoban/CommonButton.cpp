#include "CommonButton.h"
#include "Common.h"


namespace GUI{

	CommonButton::CommonButton(const sf::Texture& btnTexture, const sf::SoundBuffer& clickSound) : sprite(btnTexture)
	{
		tex::centerOrigin(sprite);
		sound.setBuffer(clickSound);
	}


	CommonButton::~CommonButton()
	{
	}

	bool CommonButton::isSelectable() const
	{
		return true;
	}

	void CommonButton::select()
	{
		Component::select();
	}

	void CommonButton::deselect()
	{
		Component::deselect();
	}

	void CommonButton::activate()
	{
		sound.setPitch(math::RandomRange(0.8f, 1.2f));
		sound.play();
		while (sound.getStatus() == sf::SoundSource::Playing);
		Component::activate();
		action();
		deactivate();
	}

	void CommonButton::setAction(std::function<void()> newAction)
	{
		action = newAction;
	}

	void CommonButton::draw(sf::RenderTarget& target, sf::RenderStates states) const
	{
		states.transform *= getTransform();
		target.draw(sprite, states);
	}

	void CommonButton::handleEvent(const sf::Event& event, const sf::RenderWindow& window)
	{
		if (event.type == sf::Event::MouseMoved)
		{
			if (isMouseHover(window))
			{
				select();
			}
			else
			{
				deselect();
			}
		}
		else if (event.type == sf::Event::MouseButtonReleased&&event.mouseButton.button == sf::Mouse::Left)
		{
			if (isMouseHover(window))
			{
				activate();
			}
		}
	}

	sf::FloatRect CommonButton::getLocalBounds() const
	{
		return sprite.getLocalBounds();
	}


	sf::FloatRect CommonButton::getGlobalBounds() const
	{
		return getTransform().transformRect(sprite.getGlobalBounds());
	}
	
	void CommonButton::setColor(const sf::Color& newColor)
	{
		sprite.setColor(newColor);
	}
}

