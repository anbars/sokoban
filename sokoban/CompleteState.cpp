#include "CompleteState.h"
#include "Button.h"
#include "Label.h"
#include "DataTables.h"

namespace states{

	CompleteState::CompleteState(StateStack& stack, Context& context) : WindowState(stack, context)
	{
		using namespace GUI;

		getContext().musicPlayer.setTrack(audio::MusicThemes::Fanfare);
		getContext().musicPlayer.Play();

		auto levelTable = LevelTable::getInstance();
		auto completedLevelID = context.levels->getCurrent(context.curScore)->getID();

		levelTable->submitScore(completedLevelID, context.curScore);

		auto levelData = levelTable->getLevelData(completedLevelID);

		auto label = new Label("Level Complete!", getContext().fonts);
		label->setPosition(backSprite.getPosition().x, backSprite.getPosition().y - backSprite.getLocalBounds().height / 2 + label->getLocalBounds().height / 2 + 40.f);
		components.pack(Component::Ptr(label));

		label = new Label("Your result: ", getContext().fonts);
		label->setPosition(backSprite.getPosition().x, backSprite.getPosition().y - backSprite.getLocalBounds().height / 2 + label->getLocalBounds().height / 2 + 90.f);
		components.pack(Component::Ptr(label));

		label = new Label(context.curScore.toString(stdScoreFormatter()), getContext().fonts);
		label->setPosition(backSprite.getPosition().x, backSprite.getPosition().y - backSprite.getLocalBounds().height / 2 + label->getLocalBounds().height / 2 + 120.f);
		components.pack(Component::Ptr(label));

		label = new Label("Best result: ", getContext().fonts);
		label->setPosition(backSprite.getPosition().x, backSprite.getPosition().y - backSprite.getLocalBounds().height / 2 + label->getLocalBounds().height / 2 + 190.f);
		components.pack(Component::Ptr(label));

		label = new Label(levelData.bestSteps.toString(stdScoreFormatter()), getContext().fonts);
		label->setPosition(backSprite.getPosition().x, backSprite.getPosition().y - backSprite.getLocalBounds().height / 2 + label->getLocalBounds().height / 2 + 220.f);
		components.pack(Component::Ptr(label));

		label = new Label("Fastest result: ", getContext().fonts);
		label->setPosition(backSprite.getPosition().x, backSprite.getPosition().y - backSprite.getLocalBounds().height / 2 + label->getLocalBounds().height / 2 + 285.f);
		components.pack(Component::Ptr(label));

		label = new Label(levelData.bestTime.toString(stdScoreFormatter()), getContext().fonts);
		label->setPosition(backSprite.getPosition().x, backSprite.getPosition().y - backSprite.getLocalBounds().height / 2 + label->getLocalBounds().height / 2 + 315.f);
		components.pack(Component::Ptr(label));

		auto button = new Button(getContext().textures, getContext().sounds, getContext().fonts, "Back to Menu");
		button->setAction([this](){
			requestStackClear();
			requestStackPush(states::ID::MainMenu);
		});
		button->setPosition(backSprite.getPosition().x + backSprite.getLocalBounds().width / 2 - button->getLocalBounds().width / 2 - 40.f, backSprite.getPosition().y + backSprite.getLocalBounds().height / 2 - button->getLocalBounds().height / 2 - 40.f);
		components.pack(Component::Ptr(button));

		if (!context.levels->isFinished())
		{
			button = new Button(getContext().textures, getContext().sounds, getContext().fonts, "Next Level");
			button->setAction([this](){
				requestStackClear();
				getContext().levels->nextLevel();
				requestStackPush(states::ID::Game);
			});
			button->setPosition(backSprite.getPosition().x - backSprite.getLocalBounds().width / 2 + button->getLocalBounds().width / 2 + 40.f, backSprite.getPosition().y + backSprite.getLocalBounds().height / 2 - button->getLocalBounds().height / 2 - 40.f);
			components.pack(Component::Ptr(button));
		}
	}



	CompleteState::~CompleteState()
	{
	}

}
