#include "State.h"
#include "Level.h"
#include "Button.h"
#include "Label.h"

namespace states{

	class GameState : public State
	{
		LevelSet::lvlPtr curLevel;
		std::vector<std::unique_ptr<GUI::Component>> components;
		GUI::Label* scoresLabel;

		void updateScores();
	public:
		GameState(StateStack& stack, Context& context);

		virtual void draw();
		virtual bool update(sf::Time deltaTime);
		virtual bool handleEvent(const sf::Event& event);
	};

}

