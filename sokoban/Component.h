
#ifndef COMPONENT_H_
#define COMPONENT_H_

#include <SFML\Graphics.hpp>
#include <memory>

namespace GUI{

	class Component: public sf::Drawable, public sf::Transformable, private sf::NonCopyable
	{
	public:
		typedef std::shared_ptr<Component> Ptr;
	private:
		bool selected;
		bool active;
	public:
		Component();
		virtual ~Component();
		virtual bool isSelectable() const = 0;
		bool isSelected() const;
		virtual void select();
		virtual void deselect();
		bool isActive() const;
		virtual void activate();
		virtual void deactivate();
		virtual void handleEvent(const sf::Event& event, const sf::RenderWindow& window) = 0;

		bool isMouseHover(const sf::RenderWindow& window) const;

		virtual void draw(sf::RenderTarget& target, sf::RenderStates states) const =0;

		virtual sf::FloatRect getLocalBounds() const = 0;
		virtual sf::FloatRect getGlobalBounds() const = 0;
	};


}
#endif