#ifndef LEVEL_SET_LIST_H_
#define LEVEL_SET_LIST_H_

#include <filesystem>
#include <vector>
#include "LevelSet.h"

using std::tr2::sys::path;
using std::tr2::sys::directory_iterator;

class LevelSetHolder
{
	std::vector<path> sets;

	sf::RenderWindow& window;
	TextureHolder& textures;
	SoundHolder& sounds;

public:
	int size() const;
	LevelSet::Ptr get(int index) const;
	std::string getSetName(int setIndex) const;
	LevelSetHolder(sf::RenderWindow& window, TextureHolder& textures, SoundHolder& sounds);
	~LevelSetHolder();
};



#endif