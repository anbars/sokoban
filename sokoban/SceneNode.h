#ifndef SCENENODE_H_
#define SCENENODE_H_

#include <memory>
#include <vector>
#include <set>
#include <SFML\Graphics.hpp>

class SceneNode:public sf::Drawable, public sf::Transformable, public sf::NonCopyable
{
public:
	typedef std::unique_ptr<SceneNode> nodePtr;
	typedef std::pair<SceneNode*, SceneNode*> nodePair;
private:
	SceneNode* parent;
	std::vector<nodePtr> children;
	std::string tag;
	virtual void drawCurrent(sf::RenderTarget& target, sf::RenderStates states) const;
	void drawChildren(sf::RenderTarget& target, sf::RenderStates states) const ;
	virtual void draw(sf::RenderTarget& target, sf::RenderStates states) const final;
	void updateChildren(sf::Time deltaTime);
	virtual void updateCurrent(sf::Time deltaTime);
	bool collision(const SceneNode& left, const SceneNode& right);
	void getNodeCollisions(SceneNode& node, std::set<nodePair>& collSet);
public:
	SceneNode(std::string tag);
	virtual ~SceneNode();
	void attachChild(nodePtr child);
	nodePtr detachChild(const SceneNode& node);
	void update(sf::Time deltaTime);
	void sceneCollisions(SceneNode& scene, std::set<nodePair>& collSet);
	void nodeCollisions(std::set<nodePair>& collSet);
	const std::string& getTag() const;

	virtual sf::FloatRect getLocalBounds() const;
	virtual sf::FloatRect getGlobalBounds() const;
};


#endif
