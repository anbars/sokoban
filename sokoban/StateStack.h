#ifndef ST_STACK_H_
#define ST_STACK_H_

#include<vector>
#include <functional>
#include "State.h"
namespace states{

	class StateStack
	{
	public:
		enum Action{
			Pop,
			Push,
			Clear
		};
		struct Change{
			Action action;
			ID stateID;
		};
	private:
		std::vector<State::stPtr> stack;
		std::vector<Change> pendingChanges;
		State::Context context;
		std::map<ID, std::function<State::stPtr()>> factories;
	public:
		explicit StateStack(State::Context context);

		template <typename T> 
		void registerState(ID stateID)
		{
			factories[stateID] = [this](){
				return State::stPtr(new T(*this, context));
			};
		}

		void update(sf::Time deltaTime);
		void draw();
		void handleEvent(sf::Event event);

		void pushState(ID stateID);
		void popState();
		void cleatStates();

		bool isEmpty() const;

	private:
		State::stPtr createState(ID stateID);
		void applyPendingChanges();
	};

}

#endif