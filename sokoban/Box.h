#ifndef BOX_H_
#define BOX_H_

#include "GameObject.h"
#include "ResourceHolder.h"

class Box :	public GameObject
{
	bool active;
	TextureHolder& textures;
public:
	Box(TextureHolder& textures);
	virtual void updateCurrent(sf::Time deltaTime) override;
	virtual bool step(const sf::Vector2f& direction) override;
	bool isActive() const;
};

#endif

