#ifndef TIT_STATE_H_
#define TIT_STATE_H_
#include "State.h"

namespace states{
	class TitleState :	public State
	{
		sf::Sprite backSprite;
		sf::Text text;
		sf::Time textTime;
		bool textVisible;
	public:
		TitleState(StateStack& stack, Context& context);
		virtual void draw();
		virtual bool update(sf::Time deltaTime);
		virtual bool handleEvent(const sf::Event& event);
	};

}

#endif
