#include "TitleState.h"

namespace states{
	TitleState::TitleState(StateStack& stack, Context& context) : State(stack, context), textVisible(true)
	{
		sf::Vector2u winSize = getContext().window.getSize();
		backSprite.setTexture(getContext().textures.get(tex::Textures::titleBack));
		text.setString("Press Any Key to Continue");
		text.setColor(sf::Color::Blue);
		text.setFont(getContext().fonts.get(font::Fonts::TitleFont));
		backSprite.move(sf::Vector2f(0, (float)(winSize.y / 2)-backSprite.getLocalBounds().height/2));
		sf::Vector2u textPos = winSize;
		textPos.y -= 75;
		textPos.x /= 2;
		textPos.x -= text.getLocalBounds().width / 2;
		text.setPosition(sf::Vector2f((float)textPos.x, (float)textPos.y));
	}

	void TitleState::draw()
	{
		getContext().window.draw(backSprite);
		if (textVisible)
		{
			getContext().window.draw(text);
		}
	}
	bool TitleState::update(sf::Time deltaTime)
	{
		textTime += deltaTime;
		if (textTime >= sf::seconds(0.5f))
		{
			textVisible = !textVisible;
			textTime = sf::Time::Zero;
		}
		return true;
	}
	bool TitleState::handleEvent(const sf::Event& event)
	{
		if (event.type == sf::Event::KeyReleased || event.type == sf::Event::MouseButtonReleased)
		{
			requestStackPop();
			requestStackPush(ID::MainMenu);
		}
		else if (event.type == sf::Event::Closed)
		{
			requestStackClear();
		}
		return true;
	}



}
