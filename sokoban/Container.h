#ifndef CONTAINER_H_
#define CONTAINER_H_

#include "Component.h"
#include <vector>

namespace GUI{
	
	class Container : public Component
	{
		std::vector<Component::Ptr> children;
		int selectedChild;
	public:
		Container();
		~Container();

		virtual bool isSelectable() const override;
		virtual void handleEvent(const sf::Event& event, const sf::RenderWindow& window) override;

		Container& pack(Component::Ptr component);

		bool hasSelection() const;

		void select(int index);
		void selectNext();
		void selectPrev();

		virtual void draw(sf::RenderTarget& target, sf::RenderStates states) const override;

		virtual sf::FloatRect getLocalBounds() const override;
		virtual sf::FloatRect getGlobalBounds() const override;

		void clear();
	};

}

#endif

