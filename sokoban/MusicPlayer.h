#ifndef MUSIC_PLAYER_H_
#define MUSIC_PLAYER_H_

#include "SFML\Audio.hpp"
#include <map>

namespace audio{

	enum MusicThemes{
		Menu,
		Main,
		Fanfare,
		MusicThemesCount
	};

	class MusicPlayer
	{
		sf::Music music;
		std::map<MusicThemes, std::string> playlist;
	public:
		MusicPlayer();
		~MusicPlayer();

		void addToPlaylist(MusicThemes id, std::string filepath);
		void setTrack(MusicThemes id);

		void Play();
		void Pause();
		void Stop();
	};


}
#endif