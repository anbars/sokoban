#ifndef DATA_TABLES_H_
#define DATA_TABLES_H_


#include "Level.h"
#include <map>
#include <memory>

class LevelTable{
public:
	typedef std::shared_ptr<LevelTable> Ptr;
private:
	std::map<Level::idType, LevelData> table;

	LevelTable(const LevelTable&) = delete;
	LevelTable();

public:
	~LevelTable();
	
	void newEntity(Level::idType newLevelID);
	void submitScore(Level::idType levelID, Score levelScore);

	LevelData getLevelData(Level::idType levelID);
	static Ptr getInstance();

};





#endif