#ifndef LABEL_H_
#define LABEL_H_
#include "Component.h"
#include "ResourceHolder.h"

namespace GUI{

	class Label : public Component
	{
		sf::Text text;
	public:

		virtual bool isSelectable() const override;
		virtual void handleEvent(const sf::Event& event, const sf::RenderWindow& window) override;

		virtual void draw(sf::RenderTarget& target, sf::RenderStates states) const override;
		Label(const std::string, const FontHolder& fonts);
		~Label();

		virtual sf::FloatRect getLocalBounds() const override;
		virtual sf::FloatRect getGlobalBounds() const override;

		void setText(std::string newText);
		void setFontSize(unsigned int size);
		void setColor(sf::Color color);
	};

}

#endif
