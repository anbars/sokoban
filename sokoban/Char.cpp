#include "Char.h"
#include <iostream>
#include <set>
#include "Common.h"


Char::Char(TextureHolder& textures, SoundHolder& sounds) :GameObject(textures.get(tex::Player), "player")
{
	stepSound.setBuffer(sounds.get(audio::Sounds::Step));
	stepSound.setVolume(50);
}


bool Char::step(const sf::Vector2f& direction)
{
	assert(direction.x >= -1 && direction.x <= 1);
	assert(direction.y >= -1 && direction.y <= 1);
	toMove = direction * 40.f*getScale().x;
	move(toMove);
	std::set<SceneNode::nodePair> nodeColls;
	nodeCollisions(nodeColls);
	for (auto& col : nodeColls)
	{
		if (col.first == this)
		{
			if (col.second->getTag() == "sprite" || col.second->getTag() == "tagetpoint")
				continue;
			else if (col.second->getTag() == "box")
			{
				auto box = static_cast<GameObject*>(col.second);
				if (box->step(direction))
					continue;
			}
		}
		else
		{
			if (col.first->getTag() == "sprite" || col.first->getTag() == "tagetpoint")
				continue;
			else if (col.first->getTag() == "box")
			{
				auto box = static_cast<GameObject*>(col.first);
				if (box->step(direction))
					continue;
			}
		}
			move(-toMove);
			toMove.x = toMove.y = 0;
			return false;
	}

		moving = true;
		stepSound.setPitch(math::RandomRange(0.9f, 1.2f));
		stepSound.play();
		targetPosition = getPosition();
		move(-toMove);
		return true;
}

void Char::updateCurrent(sf::Time deltaTime)
{
	GameObject::updateCurrent(deltaTime);
	if (stepSound.getStatus() == sf::SoundSource::Playing&&!isMoving())
	{
		stepSound.stop();
	}
}




