#include "SelectState.h"
#include "Button.h"
#include "IconButton.h"
#include "Label.h"
#include "DataTables.h"

namespace states {

	SelectState::SelectState(StateStack& stack, Context& context) : WindowState(stack, context), start(0)
	{
		updateBtns();
	}


	SelectState::~SelectState()
	{
	}

	void SelectState::updateBtns()
	{
		using namespace GUI;
		components.clear();
		
		auto levelTable = LevelTable::getInstance();

		auto label = new Label("Select Level:", getContext().fonts);
		label->setPosition(backSprite.getPosition().x, backSprite.getPosition().y - backSprite.getLocalBounds().height / 2 + label->getLocalBounds().height / 2 + 40.f);
		components.pack(Component::Ptr(label));

		for (int i = start; i < getContext().levels->size() && i<start + lvlsPerPage; i++)
		{
			auto button = new Button(getContext().textures, getContext().sounds, getContext().fonts, "Level " + std::to_string(i + 1));
			button->setAction([this, i](){
				requestStackClear();
				getContext().levels->setCurrent(i);
				requestStackPush(states::ID::Game);
			});

			sf::Vector2f pos = label->getPosition();
			pos.y += label->getLocalBounds().height + 20.f + (button->getLocalBounds().height + 5.f)*((i-start) / 2);
			pos.x = backSprite.getGlobalBounds().left + button->getLocalBounds().width / 2;
			if (i % 2)
			{
				pos.x += backSprite.getLocalBounds().width - 40.f - button->getLocalBounds().width;
			}
			else
			{
				pos.x += 40.f;
			}

			button->setPosition(pos);

			getContext().levels->setCurrent(i);
			auto levelID = getContext().levels->getCurrent(getContext().curScore)->getID();
			if (!levelTable->getLevelData(levelID).isPassed)
			{
				button->setColor(sf::Color(103,56,189,255));
			}

			components.pack(Component::Ptr(button));
		}

		if (start >= lvlsPerPage)
		{
			auto button = new IconButton(getContext().textures.get(tex::Textures::btnLeft), getContext().sounds);
			button->setAction([this](){
				start -= lvlsPerPage;
				updateRequired = true;
			});

			sf::Vector2f pos;
			pos.y = backSprite.getGlobalBounds().top + backSprite.getLocalBounds().height - button->getLocalBounds().height / 2;
			pos.x = backSprite.getGlobalBounds().left + button->getLocalBounds().height / 2;
			button->setPosition(pos);
			components.pack(Component::Ptr(button));
		}

		if (getContext().levels->size()-start >= lvlsPerPage)
		{
			auto button = new IconButton(getContext().textures.get(tex::Textures::btnRight), getContext().sounds);
			button->setAction([this](){
				start += lvlsPerPage;
				updateRequired = true;
			});

			sf::Vector2f pos;
			pos.y = backSprite.getGlobalBounds().top + backSprite.getLocalBounds().height - button->getLocalBounds().height / 2;
			pos.x = backSprite.getGlobalBounds().left +backSprite.getLocalBounds().width - button->getLocalBounds().height / 2;
			button->setPosition(pos);
			components.pack(Component::Ptr(button));
		}

		updateRequired = false;
	}

	bool SelectState::update(sf::Time deltaTime)
	{
		if (updateRequired)
			updateBtns();
		return false;
	}
}