#include "StateStack.h"

namespace states{


	StateStack::StateStack(State::Context context) : context(context)
	{
	}

	State::stPtr StateStack::createState(states::ID stateID)
	{
		auto found = factories.find(stateID);
		assert(found != factories.end());
		return found->second();
	}


	void StateStack::update(sf::Time deltaTime)
	{
		for (auto state = stack.rbegin(); state != stack.rend();state++)
		{
			if (!(*state)->update(deltaTime))
				break;
		}

		applyPendingChanges();
	}

	void StateStack::draw()
	{
		for (auto& state : stack)
		{
			state->draw();
		}
	}

	void StateStack::handleEvent(sf::Event event)
	{
		for (auto state = stack.rbegin(); state != stack.rend(); state++)
		{
			if (!(*state)->handleEvent(event))
				break;
		}

		applyPendingChanges();
	}

	void StateStack::pushState(states::ID stateID)
	{
		Change change{ Push, stateID };
		pendingChanges.push_back(change);
	}

	void StateStack::popState()
	{
		Change change{ Pop };
		pendingChanges.push_back(change);
	}

	void StateStack::cleatStates()
	{
		Change change{ Clear };
		pendingChanges.push_back(change);
	}

	void StateStack::applyPendingChanges()
	{
		for (auto change : pendingChanges)
		{
			switch (change.action)
			{
			case Push:
				stack.push_back(createState(change.stateID));
				break;
			case Pop:
				stack.pop_back();
				break;
			case Clear:
				stack.clear();
				break;
			}
		}
		pendingChanges.clear();
	}

	bool StateStack::isEmpty() const
	{
		return stack.empty();
	}

}