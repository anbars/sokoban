#include "Component.h"

namespace GUI{

	Component::Component() : selected(false), active(false)
	{
	}


	Component::~Component()
	{
	}

	bool Component::isActive() const
	{
		return active;
	}

	void Component::activate()
	{
		active = true;
	}

	void Component::deactivate()
	{
		active = false;
	}

	bool Component::isSelected() const
	{
		return selected;
	}

	void Component::select()
	{
		selected = true;
	}

	void Component::deselect()
	{
		selected = false;
	}

	bool Component::isMouseHover(const sf::RenderWindow& window) const
	{
		auto mousePos = sf::Vector2f(sf::Mouse::getPosition(window));
		auto boundingRect = getGlobalBounds();
		return boundingRect.contains(mousePos);
	}
}