#include "State.h"
#include "StateStack.h"

namespace states{

	State::State(StateStack& stack, Context& context): stack(stack), context(context)
	{
	}


	State::~State()
	{
	}


	void State::requestStackPush(states::ID stateID)
	{
		stack.pushState(stateID);
	}

	void State::requestStackPop()
	{
		stack.popState();
	}

	void State::requestStackClear()
	{
		stack.cleatStates();
	}

	State::Context& State::getContext() const
	{
		return context;
	}
}
