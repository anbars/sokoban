#ifndef LEVEL_H_
#define LEVEL_H_

#include <SFML\Graphics.hpp>
#include "ResourceHolder.h"
#include "SceneNode.h"
#include "Wall.h"
#include "Box.h"
#include "Char.h"
#include "TargetPoint.h"
#include <array>
#include <vector>
#include <memory>
#include <tuple>
#include <stack>

enum Layer
{
	Back,
	Active,
	LayerCount
};

struct Score;
struct scoreFormatter{
	virtual std::string formatScore(const Score& score) const = 0;
};

struct Score{
	int steps;
	sf::Time lvlTime;
	Score() :steps(0), lvlTime(sf::Time::Zero){};
	Score(const Score& that)
	{
		steps = that.steps;
		lvlTime = that.lvlTime;
	}
	void reset(){
		steps = 0;
		lvlTime = sf::Time::Zero;
	}
	std::string toString(const scoreFormatter& format) const
	{
		return format.formatScore(*this);
	}
	friend std::ostream& operator<<(std::ostream& os, const Score& Score);
	friend std::istream& operator>>(std::istream& is, Score& Score);
};
std::ostream& operator<<(std::ostream& os, const Score& score);
std::istream& operator>>(std::istream& is, Score& score);

struct LevelData{
	bool isPassed;
	Score bestSteps;
	Score bestTime;
	LevelData() :isPassed(false){};
};

std::ostream& operator<<(std::ostream& os, const LevelData& levelData);
std::istream& operator>>(std::istream& is, LevelData& levelData);


class Level
{
public:
	typedef std::vector<std::vector<tex::Textures>> Map;
	typedef std::pair<std::string, int> idType;
private:
	idType id;

	SceneNode scene;
	std::array<SceneNode*, LayerCount> sceneLayers;
	std::vector<Box*> boxes;
	Map levelMap;

	sf::RenderWindow& window;
	TextureHolder& textures;
	SoundHolder& sounds;

	Score& curScore;

	Char* player;

	typedef std::vector<std::pair<GameObject*, sf::Vector2f>> StepBackup;
	std::stack<StepBackup> stepBackups;
	void makeStepBackup();
	
	void handlePlayerInput();
public:
	void buildScene();
	Level(sf::RenderWindow& window, TextureHolder& textures, SoundHolder& sounds, const Map& levelMap, Score& score, idType id);
	void draw();
	void update(sf::Time deltaTime);
	static Map loadFromFile(std::string filename);
	bool isDone() const;

	sf::FloatRect getLocalBounds() const;

	idType getID() const;
	void revertLastStep();
};



class stdScoreFormatter: public scoreFormatter{
public:
	virtual std::string formatScore(const Score& score) const override;
};

#endif
