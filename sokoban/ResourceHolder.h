#ifndef RESOURCEHOLDER_H_
#define RESOURCEHOLDER_H_

#include <map>
#include <string>
#include <exception>
#include <memory>
#include <assert.h>

#include<SFML\Graphics.hpp>
#include<SFML\Audio.hpp>

template <typename Resource, typename Key>
class ResourceHolder{
	typedef std::unique_ptr<Resource> rptr;
	std::map<Key, rptr> resourceMap;
public:
	void load(Key id, const std::string &filename)
	{
		rptr resource(new Resource);
		if (!(resource->loadFromFile(filename)))
		{
			throw std::runtime_error("ResourceHolder::Load->Failed to load file " + filename);
		}

		auto inserted = resourceMap.insert(std::make_pair(id, move(resource)));
		assert(inserted.second);
	}

	//������������� ������ load ��� ��������, ������� ��������� 2 ��������� � ������� load
	template <typename Parameter>
	void load(Key id, const std::string &filename, const Parameter& secondParam)
	{
		rptr resource(new Resource);
		if (!Resource->loadFromFile(filename, secondParam))
		{
			throw std::runtime_error("ResourceHolder::Load->Failed to load file " + filename);
		}
		auto inserted = resourceMap.insert(std::make_pair(id, move(resource)));
		assert(inserted.second);
	}

	Resource& get(Key id)
	{
		auto found = resourceMap.find(id);
		assert(found != resourceMap.end());
		return *found->second;
	}

	const Resource& get(Key id) const
	{
		auto found = resourceMap.find(id);
		assert(found != resourceMap.end());
		return *found->second;
	}
};

namespace tex{
	enum Textures { 
		Player, 
		Box, 
		BoxActive, 
		TargetPlace, 
		Wall, 
		Flour, 
		Background,
		titleBack,
		stdBtn,
		stdBtnOn,
		btnLeft,
		btnRight, 
		btnPause,
		btnReset,
		dialogBlack, 
		dialogBlackSm, 
		TextureCount };
	
	template <typename T>
	inline void centerOrigin(T& item)
	{
		sf::Vector2f newOrigin = sf::Vector2f(item.getLocalBounds().width / 2, item.getLocalBounds().height/2);
		item.setOrigin(newOrigin);
	}
}

namespace font{
	enum Fonts{TitleFont, LabelFont, FontCount};
}

namespace audio{
	enum Sounds{Step, Click, SoundsCount};
}

typedef ResourceHolder<sf::Texture, tex::Textures> TextureHolder;
typedef ResourceHolder<sf::Font, font::Fonts> FontHolder;
typedef ResourceHolder<sf::SoundBuffer, audio::Sounds> SoundHolder;

#endif