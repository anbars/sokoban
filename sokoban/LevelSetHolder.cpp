#include "LevelSetHolder.h"
#include <assert.h>

LevelSetHolder::LevelSetHolder(sf::RenderWindow& window, TextureHolder& textures, SoundHolder& sounds) : window(window), textures(textures), sounds(sounds)
{
	directory_iterator iter("Resources/Levels");
	assert(iter!=directory_iterator());

	while (iter != directory_iterator())
	{
		if (iter->status().type() == std::tr2::sys::regular_file)
		{
			sets.push_back(iter->path());
		}
		iter++;
	}
}

std::string LevelSetHolder::getSetName(int setIndex) const
{
	std::string name = sets[setIndex].filename();
	name.resize(name.size() - 4);
	return name;
}


LevelSet::Ptr LevelSetHolder::get(int index) const
{
	LevelSet::Ptr levelSet(new LevelSet(window, textures,sounds, sets[index]));
	return levelSet;
}

int LevelSetHolder::size() const
{
	return sets.size();
}

LevelSetHolder::~LevelSetHolder()
{
}
