#include "SpriteNode.h"
#include "ResourceHolder.h"
SpriteNode::SpriteNode(const Texture& texture) :sprite(texture), SceneNode("sprite")
{
	tex::centerOrigin(sprite);
}

SpriteNode::SpriteNode(const Texture& texture, const IntRect& rect) : sprite(texture, rect), SceneNode("sprite")
{

}

void SpriteNode::drawCurrent(sf::RenderTarget& target, sf::RenderStates states) const
{
	target.draw(sprite, states);
}
