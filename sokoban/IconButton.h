#ifndef ICON_BUTTON_H_
#define ICON_BUTTON_H_

#include "CommonButton.h"

namespace GUI{

	class IconButton :	public CommonButton
	{
		sf::Keyboard::Key hotKey;
	public:
		IconButton(const sf::Texture& btnTexture, const SoundHolder& sounds, sf::Keyboard::Key hotKey = sf::Keyboard::KeyCount);
		~IconButton();

		virtual void select() override;
		virtual void deselect() override;

		virtual void handleEvent(const sf::Event& event, const sf::RenderWindow& window) override;
	};


}
#endif