#include "SetSelectState.h"
#include "Button.h"
#include "IconButton.h"
#include "Label.h"

namespace states{

	SetSelectState::SetSelectState(StateStack& stack, Context& context) : WindowState(stack, context), start(0), levelSets(getContext().window, getContext().textures,getContext().sounds)
	{
		updateBtns();
	}


	SetSelectState::~SetSelectState()
	{
	}

	GUI::Component::Ptr SetSelectState::createSetButton(int setIndex)
	{
		using namespace GUI;
		
		auto button = new Button(getContext().textures,getContext().sounds, getContext().fonts, levelSets.getSetName(setIndex));
		button->setAction([this, setIndex](){
			getContext().levels = levelSets.get(setIndex);
			requestStackPush(states::ID::Select);
		});


		return Component::Ptr(button);
	}

	void SetSelectState::updateBtns()
	{
		using namespace GUI;
		components.clear();

		auto label = new Label("Select Collection:", getContext().fonts);
		label->setPosition(backSprite.getPosition().x, backSprite.getPosition().y - backSprite.getLocalBounds().height / 2 + label->getLocalBounds().height / 2 + 40.f);
		components.pack(Component::Ptr(label));

		for (int i = start; i < levelSets.size() && i<start + setsPerPage; i++)
		{
			auto button = createSetButton(i);
			sf::Vector2f pos = label->getPosition();
			pos.y += label->getLocalBounds().height + 20.f + (button->getLocalBounds().height + 5.f)*((i - start) / 2);
			pos.x = backSprite.getGlobalBounds().left + button->getLocalBounds().width / 2;
			if (i % 2)
			{
				pos.x += backSprite.getLocalBounds().width - 40.f - button->getLocalBounds().width;
			}
			else
			{
				pos.x += 40.f;
			}

			button->setPosition(pos);

			components.pack(Component::Ptr(button));
		}

		if (start >= setsPerPage)
		{
			auto button = new IconButton(getContext().textures.get(tex::Textures::btnLeft), getContext().sounds);
			button->setAction([this](){
				start -= setsPerPage;
				updateRequired = true;
			});

			sf::Vector2f pos;
			pos.y = backSprite.getGlobalBounds().top + backSprite.getLocalBounds().height - button->getLocalBounds().height / 2;
			pos.x = backSprite.getGlobalBounds().left + button->getLocalBounds().height / 2;
			button->setPosition(pos);
			components.pack(Component::Ptr(button));
		}

		if (levelSets.size() - start >= setsPerPage)
		{
			auto button = new IconButton(getContext().textures.get(tex::Textures::btnRight), getContext().sounds);
			button->setAction([this](){
				start += setsPerPage;
				updateRequired = true;
			});

			sf::Vector2f pos;
			pos.y = backSprite.getGlobalBounds().top + backSprite.getLocalBounds().height - button->getLocalBounds().height / 2;
			pos.x = backSprite.getGlobalBounds().left + backSprite.getLocalBounds().width - button->getLocalBounds().height / 2;
			button->setPosition(pos);
			components.pack(Component::Ptr(button));
		}

		updateRequired = false;
	}

	bool SetSelectState::update(sf::Time deltaTime)
	{
		if (updateRequired)
			updateBtns();
		return false;
	}
}