#include "DataTables.h"
#include <fstream>

LevelTable::LevelTable()
{
	std::ifstream dataFile;
	dataFile.open("Resources/Data/leveltable");
	if (dataFile.is_open())
	{
		Level::idType id;
		LevelData data;
		while (dataFile >> id.first && dataFile >> id.second && dataFile >> data)
		{
			table.insert(std::make_pair(id, data));
		}
		if (!dataFile.eof())
		{
			throw std::runtime_error("LevelTable: Wrong leveltable file format!");
		}
	}
}

LevelTable::~LevelTable()
{
	std::ofstream dataFile;
	dataFile.open("Resources/Data/leveltable");

	if (!dataFile.is_open())
	{
		throw std::runtime_error("LevelTable: Cannot save to file: Cannot open file leveldata for writing!");
	}
	
	for (auto entity : table)
	{
		dataFile << entity.first.first << ' ' << entity.first.second << ' ' << entity.second << '\n';
	}
}

void LevelTable::newEntity(Level::idType newLevelID)
{
	table.insert(std::make_pair(newLevelID, LevelData()));
}

void LevelTable::submitScore(Level::idType levelID, Score levelScore)
{
	LevelData& levelData = table.at(levelID);
	if (levelScore.steps < levelData.bestSteps.steps || !levelData.isPassed)
	{
		levelData.bestSteps = levelScore;
	}

	if (levelScore.lvlTime < levelData.bestTime.lvlTime || !levelData.isPassed)
	{
		levelData.bestTime = levelScore;
	}

	levelData.isPassed = true;
}

LevelTable::Ptr LevelTable::getInstance()
{
	static auto instance = Ptr(new LevelTable());

	return instance;
}

LevelData LevelTable::getLevelData(Level::idType levelID)
{
	return table.at(levelID);
}

