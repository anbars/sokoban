#include "Level.h"
#include "SpriteNode.h"
#include "Char.h"
#include <fstream>
#include <string>

using tex::Textures;
Level::Level(sf::RenderWindow& window, TextureHolder& textures, SoundHolder& sounds, const Map& levelMap, Score& score, idType id) :window(window), textures(textures), levelMap(levelMap), scene("empty"), curScore(score), id(id), sounds(sounds)
{
	player = nullptr;
}

void Level::buildScene()
{
	for (std::size_t i=0; i < LayerCount; i++)
	{
		SceneNode::nodePtr layer(new SceneNode("empty"));
		sceneLayers[i] = layer.get();
		scene.attachChild(std::move(layer));
	}
	

	sf::Vector2f scaleFactor(1.f,1.f);
	sf::Vector2f levelSize = sf::Vector2f(getLocalBounds().width, getLocalBounds().height);
	float scaleFactorX = window.getView().getSize().x / levelSize.x;
	float scaleFactorY = window.getView().getSize().y / levelSize.y;

	if (scaleFactorX <1 || scaleFactorY < 1)
	{
		scaleFactor.x = scaleFactor.y = std::min(scaleFactorX, scaleFactorY)*1.01f;//1.01-��������� ��������� �����, ��� �� �������� �������� ����� ����������
	}

	sf::Texture& texture = textures.get(Textures::Background);
	sf::IntRect textureRect = sf::IntRect(sf::Vector2i(0, 0), sf::Vector2i(window.getView().getSize()));
	std::unique_ptr<SpriteNode> backSprite(new SpriteNode(texture, textureRect));
	backSprite->setPosition(0, 0);
	sceneLayers[Back]->attachChild(std::move(backSprite));
	sf::Vector2f posCorrection;
	posCorrection.x = window.getView().getSize().x / 2 - getLocalBounds().width / 2*scaleFactor.x;
	posCorrection.y = window.getView().getSize().y / 2 - getLocalBounds().height / 2*scaleFactor.y;
	for (int i = 0; i < levelMap.size(); i++)
	{
		bool rowBegin = true;
		for (int j = 0; j < levelMap[i].size(); j++)
		{
			if (levelMap[i][j] != tex::Textures::Flour)
				rowBegin = false;
			sf::Vector2i pixelPosition(j * 40.f*scaleFactor.x, i * 40.f*scaleFactor.y);
			sf::Vector2f position = window.mapPixelToCoords(pixelPosition);
			position += posCorrection;
			//������� ��� �� ������ ������ �����
			if (!rowBegin)
			{
				SceneNode::nodePtr flourNode(new SpriteNode(textures.get(Textures::Flour)));
				flourNode->setScale(scaleFactor);
				flourNode->setPosition(position);
				sceneLayers[Layer::Back]->attachChild(std::move(flourNode));
			}
			SceneNode::nodePtr newNode;
			switch (levelMap[i][j])
			{
			case Textures::Box:
				newNode = SceneNode::nodePtr(new Box(textures));
				boxes.push_back(static_cast<Box*>(newNode.get()));
				break;
			case Textures::Wall:
				newNode = SceneNode::nodePtr(new Wall(textures));
				break;
			case Textures::Player:
				newNode = SceneNode::nodePtr(new Char(textures,sounds));
				player = static_cast<Char*>(newNode.get());
				break;
			case Textures::TargetPlace:
				newNode = SceneNode::nodePtr(new TargetPoint(textures));
				newNode->setPosition(position);
				newNode->setScale(scaleFactor);
				sceneLayers[Back]->attachChild(std::move(newNode));
				continue;
			case Textures::BoxActive:
				newNode = SceneNode::nodePtr(new TargetPoint(textures));
				newNode->setPosition(position);
				newNode->setScale(scaleFactor);
				sceneLayers[Back]->attachChild(std::move(newNode));
				newNode = SceneNode::nodePtr(new Box(textures));
				boxes.push_back(static_cast<Box*>(newNode.get()));
				break;
			case Textures::TextureCount://����� �� �������� ����
				newNode = SceneNode::nodePtr(new TargetPoint(textures));
				newNode->setPosition(position);
				newNode->setScale(scaleFactor);
				sceneLayers[Back]->attachChild(std::move(newNode));
				newNode = SceneNode::nodePtr(new Char(textures, sounds));
				player = static_cast<Char*>(newNode.get());
				break;
			case Textures::Flour:
				continue;

			}
			assert(newNode);
			newNode->setScale(scaleFactor);
			newNode->setPosition(position);
			sceneLayers[Active]->attachChild(std::move(newNode));
		}
	}
}

void Level::draw()
{
	window.draw(scene);
}

void Level::update(sf::Time deltaTime)
{
	handlePlayerInput();
	scene.update(deltaTime);
}

Level::Map Level::loadFromFile(std::string filename)
{
	using tex::Textures;
	std::ifstream file;
	file.open(filename);
	assert(file.is_open());
	Map levelMap;
	std::string line;
	while (std::getline(file, line))
	{
		std::vector<Textures> row;
		for (char c : line)
		{
			switch (c)
			{
			case '#':
				row.push_back(Textures::Wall);
				break;
			case '@':
				row.push_back(Textures::Player);
				break;
			case '$':
				row.push_back(Textures::Box);
				break;
			case '.':
				row.push_back(Textures::TargetPlace);
				break;
			default:
				row.push_back(Textures::Flour);
				break;
			}
		}
		levelMap.push_back(row);
	}
	return levelMap;
}

bool Level::isDone() const
{
	for (auto box : boxes)
	{
		if (!(box->isActive()))
			return false;
	}
	return true;
}

sf::FloatRect Level::getLocalBounds() const
{
	float heigt = levelMap.size() * 40;
	float width = levelMap[0].size();
	for (int i = 1; i < levelMap.size(); i++)
	{
		if (levelMap[i].size()>width)
			width = levelMap[i].size();
	}
	width *= 40;
	return sf::FloatRect(0, 0, width, heigt);
}

Level::idType Level::getID() const
{
	return id;
}

void Level::handlePlayerInput()
{
	using sf::Keyboard;
	if (!player->isMoving())
	{
		sf::Vector2f directionToMove;
		bool haveToMove = false;
		if (Keyboard::isKeyPressed(Keyboard::Left))
		{
			directionToMove.x = -1;
			haveToMove = true;
		}
		else if (Keyboard::isKeyPressed(Keyboard::Right))
		{
			directionToMove.x = 1;
			haveToMove = true;
		}
		else if (Keyboard::isKeyPressed(Keyboard::Down))
		{
			directionToMove.y = 1;
			haveToMove = true;
		}
		else if (Keyboard::isKeyPressed(Keyboard::Up))
		{
			directionToMove.y = -1;
			haveToMove = true;
		}

		if (haveToMove)
		{
			if (player->step(directionToMove))
			{
				curScore.steps++;
				makeStepBackup();
			}
		}
	}
}

void Level::makeStepBackup()
{
	StepBackup stepBackup;
	stepBackup.push_back(std::make_pair(player, player->getPosition()));
	for (auto box : boxes)
	{
		if (box->isMoving())
			stepBackup.push_back(std::make_pair(box, box->getPosition()));
	}
	stepBackups.push(stepBackup);
}

void Level::revertLastStep()
{
	if (!stepBackups.empty())
	{
		auto stepBackup = stepBackups.top();
		for (auto& backupItem : stepBackup)
		{
			backupItem.first->setPosition(backupItem.second);
		}
		stepBackups.pop();
		curScore.steps++;
	}
}

std::string stdScoreFormatter::formatScore(const Score& score) const
{
	int min = (int)score.lvlTime.asSeconds() / 60;
	int sec = (int)score.lvlTime.asSeconds() % 60;

	std::string formattedScores = "";

	formattedScores += "LevelTime: ";
	if (min < 10) formattedScores += '0';
	formattedScores += std::to_string(min);
	formattedScores += ":";
	if (sec < 10) formattedScores += '0';
	formattedScores += std::to_string(sec);

	formattedScores += "\nSteps: ";
	formattedScores += std::to_string(score.steps);
	
	return formattedScores;
}

std::ostream& operator<<(std::ostream& os, const Score& score)
{
	os << score.lvlTime.asSeconds() << ' ' << score.steps;
	return os;
}

std::istream& operator>>(std::istream& is, Score& score)
{
	float seconds;
	is >> seconds;
	score.lvlTime = sf::seconds(seconds);
	is >> score.steps;
	return is;
}

std::ostream& operator<<(std::ostream& os, const LevelData& levelData)
{
	os << levelData.isPassed << ' ' << levelData.bestSteps << ' ' << levelData.bestTime;
	return os;
}
std::istream& operator>>(std::istream& is, LevelData& levelData)
{
	is >> levelData.isPassed >> levelData.bestSteps >> levelData.bestTime;
	return is;
}