#ifndef PAUSE_STATE_H_
#define PAUSE_STATE_H_

#include "State.h"
#include "Container.h"

namespace states{

	class PauseState : public State
	{
		GUI::Container components;
		sf::Sprite backSprite;
		sf::RectangleShape shadowRect;
	public:
		PauseState(StateStack& stack, Context& context);
		~PauseState();
		virtual void draw() override;
		virtual bool update(sf::Time deltaTime) override;
		virtual bool handleEvent(const sf::Event& event) override;
	};

}
#endif
