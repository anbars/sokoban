#ifndef COMPLETE_STATE_H_
#define COMPLETE_STATE_H_

#include "WindowState.h"
#include "Container.h"

namespace states{
	class CompleteState : public WindowState
	{
	public:
		CompleteState(StateStack& stack, Context& context);
		~CompleteState();
	};

}

#endif