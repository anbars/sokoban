#include "WindowState.h"

namespace states{

	WindowState::WindowState(StateStack& stack, Context& context) : State(stack, context), backSprite(context.textures.get(tex::Textures::dialogBlack))
	{
		tex::centerOrigin(backSprite);
		backSprite.setPosition(context.window.getView().getSize() / 2.f);

		shadowRect.setSize(context.window.getView().getSize());
		shadowRect.setFillColor(sf::Color(0, 0, 0, 150));
	}


	WindowState::~WindowState()
	{
	}

	void WindowState::draw()
	{
		getContext().window.draw(shadowRect);
		getContext().window.draw(backSprite);
		getContext().window.draw(components);
	}

	bool WindowState::update(sf::Time deltaTime)
	{
		return false;
	}

	bool WindowState::handleEvent(const sf::Event& event)
	{
		if (event.type == sf::Event::Closed)
		{
			requestStackPush(states::ID::Exit);
		}
		else if (event.type == sf::Event::KeyReleased&&event.key.code == sf::Keyboard::Escape)
		{
			requestStackPop();
		}
		else
		{
			components.handleEvent(event, getContext().window);
		}

		return false;
	}
}
