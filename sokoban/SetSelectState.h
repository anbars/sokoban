#ifndef SET_SELECT_STATE_H_
#define SET_SELECT_STATE_H_


#include "WindowState.h"
#include "LevelSetHolder.h"

namespace states{

	class SetSelectState :	public WindowState
	{
		int start;
		void updateBtns();
		bool updateRequired;

		LevelSetHolder levelSets;

		enum{ setsPerPage = 12 };

		GUI::Component::Ptr createSetButton(int setIndex);

	public:
		SetSelectState(StateStack& stack, Context& context);
		virtual bool update(sf::Time deltaTime) override;
		~SetSelectState();
	};


}
#endif