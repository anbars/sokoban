#include "GameState.h"
#include "Label.h"
#include "IconButton.h"

namespace states{

	GameState::GameState(StateStack& stack, Context& context) : State(stack, context), curLevel(context.levels->getCurrent(context.curScore))
	{
		context.curScore.reset();
		curLevel->buildScene();
		auto button = new GUI::IconButton(getContext().textures.get(tex::Textures::btnReset), getContext().sounds, sf::Keyboard::R);
		button->setAction([this](){
			curLevel->revertLastStep();
		});
		button->setPosition(sf::Vector2f(context.window.getView().getSize().x - button->getLocalBounds().width / 2, context.window.getView().getSize().y - button->getLocalBounds().height / 2));
		components.push_back(std::unique_ptr<GUI::IconButton>(button));

		button = new GUI::IconButton(getContext().textures.get(tex::Textures::btnPause), getContext().sounds, sf::Keyboard::Escape);
		button->setAction([this](){
			requestStackPush(states::ID::Pause);
		});
		button->setPosition(sf::Vector2f(context.window.getView().getSize().x - button->getLocalBounds().width / 2, context.window.getView().getSize().y - button->getLocalBounds().height / 2*3));
		components.push_back(std::unique_ptr<GUI::IconButton>(button));

		auto label = new GUI::Label("", getContext().fonts);
		label->setPosition(0, 0);
		label->setFontSize(20);
		label->setColor(sf::Color::Black);
		scoresLabel = label;
		components.push_back(std::unique_ptr<GUI::Label>(label));
		updateScores();

		getContext().musicPlayer.setTrack(audio::MusicThemes::Main);
		getContext().musicPlayer.Play();
	}

	void GameState::draw()
	{
		curLevel->draw();
		for (auto& button : components)
		{
			getContext().window.draw(*button);
		}
	}

	bool GameState::update(sf::Time deltatime)
	{
		getContext().curScore.lvlTime += deltatime;
		updateScores();
		curLevel->update(deltatime);
		if (curLevel->isDone())
		{
			requestStackPush(states::ID::Complete);
		}
		return true;
	}

	bool GameState::handleEvent(const sf::Event& event)
	{
		switch (event.type)
		{
		case sf::Event::Closed:
			requestStackPush(states::ID::Exit);
			break;
		case sf::Event::LostFocus:
			requestStackPush(states::ID::Pause);
			break;
		default:
			for (auto& component : components)
			{
				component->handleEvent(event, getContext().window);
			}
		}
		return true;
	}

	void GameState::updateScores()
	{
		scoresLabel->setText(getContext().curScore.toString(stdScoreFormatter()));
	}
}
