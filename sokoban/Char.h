#ifndef CHAR_H_
#define CHAR_H_

#include "GameObject.h"
#include "ResourceHolder.h"

class Char : public GameObject
{
	sf::Sound stepSound;
public:
	Char(TextureHolder& textures, SoundHolder& sounds);
	virtual bool step(const sf::Vector2f &direction) override;
	virtual void updateCurrent(sf::Time deltaTime) override;
};

#endif

