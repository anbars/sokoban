#include "PauseState.h"
#include "Button.h"
#include "Label.h"

namespace states{

	PauseState::PauseState(StateStack& stack, Context& context) : State(stack, context), backSprite(context.textures.get(tex::Textures::dialogBlackSm))
	{
		using namespace GUI;
		getContext().musicPlayer.Pause();

		tex::centerOrigin(backSprite);
		backSprite.setPosition(context.window.getView().getSize() / 2.f);

		shadowRect.setSize(context.window.getView().getSize());
		shadowRect.setFillColor(sf::Color(0,0,0,150));

		auto label = new Label("Game Paused", getContext().fonts);
		label->setPosition(backSprite.getPosition().x, backSprite.getPosition().y - backSprite.getLocalBounds().height/2 + label->getLocalBounds().height / 2+20.f);
		components.pack(Component::Ptr(label));

		auto button = new Button(getContext().textures, getContext().sounds, getContext().fonts, "Resume");
		button->setAction([this](){
			requestStackPop();
		});
		button->setPosition(backSprite.getPosition().x,label->getPosition().y+button->getLocalBounds().height/2+20.f);
		components.pack(Component::Ptr(button));

		button = new Button(getContext().textures, getContext().sounds, getContext().fonts, "Restart Level");
		button->setAction([this](){
			requestStackClear();
			requestStackPush(states::ID::Game);
		});
		button->setPosition(backSprite.getPosition().x, label->getPosition().y + button->getLocalBounds().height / 2*3 + 20.f);
		components.pack(Component::Ptr(button));

		button = new Button(getContext().textures, getContext().sounds, getContext().fonts, "Back to Menu");
		button->setAction([this](){
			requestStackClear();
			requestStackPush(states::ID::MainMenu);
		});
		button->setPosition(backSprite.getPosition().x, label->getPosition().y + button->getLocalBounds().height / 2 * 5 + 20.f);
		components.pack(Component::Ptr(button));
	}


	PauseState::~PauseState()
	{
		getContext().musicPlayer.Play();
	}

	void PauseState::draw()
	{
		getContext().window.draw(shadowRect);
		getContext().window.draw(backSprite);
		getContext().window.draw(components);
	}

	bool PauseState::update(sf::Time deltaTime)
	{
		return false;
	}

	bool PauseState::handleEvent(const sf::Event& event)
	{
		
		if (event.type == sf::Event::Closed)
		{
			requestStackPush(states::ID::Exit);
		}
		else if (event.type == sf::Event::KeyReleased&&event.key.code == sf::Keyboard::Escape)
		{
			requestStackPop();
		}
		else
		{
			components.handleEvent(event, getContext().window);
		}

		return false;
	}


}