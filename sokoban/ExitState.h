#ifndef EXIT_STATE_H_
#define EXIT_STATE_H_

#include "State.h"
#include "Container.h"

namespace states{

	class ExitState : public State
	{
		GUI::Container components;
		sf::Sprite backSprite;
		sf::RectangleShape shadowRect;
	public:
		ExitState(StateStack& stack, Context& context);
		~ExitState();
		virtual void draw() override;
		virtual bool update(sf::Time deltaTime) override;
		virtual bool handleEvent(const sf::Event& event) override;
	};

}
#endif
