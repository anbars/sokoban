#include "Application.h"
#include "State.h"
#include "GameState.h"
#include "TitleState.h"
#include "MenuState.h"
#include "PauseState.h"
#include "ExitState.h"
#include "CompleteState.h"
#include "SelectState.h"
#include "SetSelectState.h"

Application::Application() :mWindow(sf::VideoMode(1280, 720), "Sokoban"), stateStack(states::State::Context{ textures, mWindow, fonts, sounds, musicPlayer})
{
	loadTextures();
	loadFonts();
	loadSounds();
	setMusic();
	registerStates();
	stateStack.pushState(states::ID::Title);
}


Application::~Application()
{
}

void Application::loadTextures()
{
	using tex::Textures;
	textures.load(Textures::Player, "Resources/Textures/player.png");
	textures.load(Textures::Box, "Resources/Textures/box.png");
	textures.load(Textures::BoxActive, "Resources/Textures/boxActive.png");
	textures.load(Textures::TargetPlace, "Resources/Textures/targetPlace.png");
	textures.load(Textures::Wall, "Resources/Textures/wall.png");
	textures.load(Textures::Background, "Resources/Textures/back.png");
	textures.get(Textures::Background).setRepeated(true);
	textures.load(Textures::Flour, "Resources/Textures/flour.png");
	textures.load(Textures::titleBack, "Resources/Textures/titleBack.png");
	textures.load(Textures::stdBtn, "Resources/Textures/GUI/standard-button-off.png");
	textures.load(Textures::stdBtnOn, "Resources/Textures/GUI/standard-button-on.png");
	textures.load(Textures::dialogBlack, "Resources/Textures/GUI/black-dialog.png");
	textures.load(Textures::dialogBlackSm, "Resources/Textures/GUI/black-dialog-sm.png");
	textures.load(Textures::btnLeft, "Resources/Textures/GUI/020_btn_L.png");
	textures.load(Textures::btnRight, "Resources/Textures/GUI/021_btn_R.png");
	textures.load(Textures::btnPause, "Resources/Textures/GUI/i_b001.png");
	textures.load(Textures::btnReset, "Resources/Textures/GUI/i_b025.png");
}

void Application::loadFonts()
{
	using font::Fonts;
	fonts.load(Fonts::TitleFont, "Resources/Fonts/neuropol x rg.ttf");
	fonts.load(Fonts::LabelFont, "Resources/Fonts/Orbitron Light.ttf");
}

void Application::loadSounds()
{
	using audio::Sounds;
	sounds.load(Sounds::Click, "Resources/Audio/Click.wav");
	sounds.load(Sounds::Step, "Resources/Audio/Step.wav");
}

void Application::setMusic()
{
	using audio::MusicThemes;
	musicPlayer.addToPlaylist(MusicThemes::Main, "Resources/Audio/MainTheme.wav");
	musicPlayer.addToPlaylist(MusicThemes::Menu, "Resources/Audio/MenuTheme.wav");
	musicPlayer.addToPlaylist(MusicThemes::Fanfare, "Resources/Audio/fanfare.wav");
}

void Application::registerStates()
{
	using namespace states;
	stateStack.registerState<TitleState>(ID::Title);
	stateStack.registerState<GameState>(ID::Game);
	stateStack.registerState<MenuState>(ID::MainMenu);
	stateStack.registerState<PauseState>(ID::Pause);
	stateStack.registerState<ExitState>(ID::Exit);
	stateStack.registerState<CompleteState>(ID::Complete);
	stateStack.registerState<SelectState>(ID::Select);
	stateStack.registerState<SetSelectState>(ID::SetSelect);
}

void Application::render()
{
	mWindow.clear();
	stateStack.draw();
	mWindow.display();
}

void Application::processEvents()
{
	sf::Event event;
	while (mWindow.pollEvent(event))
	{
		if (event.type == sf::Event::Resized)
		{
			sf::FloatRect visibleArea(0, 0, event.size.width, event.size.height);
			mWindow.setView(sf::View(visibleArea));
		}
		else
		{
			stateStack.handleEvent(event);
		}
	}
		
}

void Application::update(sf::Time deltaTime)
{
	stateStack.update(deltaTime);
}

void Application::run()
{
	sf::Clock frameClock;
	do
	{
		sf::Time deltaTime = frameClock.restart();
		processEvents();
		update(deltaTime);
		render();
	} while (!stateStack.isEmpty());
	mWindow.close();
}
