#ifndef STATE_H_
#define STATE_H_

#include <memory>
#include <SFML\Graphics.hpp>

#include "ResourceHolder.h"
#include "LevelSet.h"
#include "MusicPlayer.h"

namespace states{
	class StateStack;
	enum ID{
		Title,
		MainMenu,
		Game,
		Pause,
		Exit,
		Complete,
		Select,
		SetSelect
	};
	class State
	{
	public:
		struct Context{
			TextureHolder& textures;
			sf::RenderWindow& window;
			FontHolder& fonts;
			SoundHolder& sounds;
			audio::MusicPlayer& musicPlayer;
			LevelSet::Ptr levels;
			Score curScore;
		};
	private:
		StateStack& stack;
		Context& context;
	protected:
		void requestStackPush(states::ID stateID);
		void requestStackPop();
		void requestStackClear();
		Context& getContext() const;
	public:

		typedef std::unique_ptr<State> stPtr;
		State(StateStack& stack, Context& context);
		virtual ~State();

		virtual void draw() = 0;
		virtual bool update(sf::Time deltaTime) = 0;
		virtual bool handleEvent(const sf::Event& event) = 0;
	};

}
#endif