#ifndef SPRITENODE_H_
#define SPRITENODE_H_

#include "SceneNode.h"

using sf::Texture;
using sf::IntRect;
using sf::Sprite;

class SpriteNode :public SceneNode
{
	Sprite sprite;
	virtual void drawCurrent(sf::RenderTarget& target, sf::RenderStates states) const override;
public:
	SpriteNode(const Texture& texture);
	SpriteNode(const Texture& texture, const IntRect& rect);
};

#endif

