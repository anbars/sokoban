#include "SceneNode.h"
#include <algorithm>
#include "ResourceHolder.h"
#include <assert.h>


SceneNode::SceneNode(std::string tag) :tag(tag)
{
	tex::centerOrigin(*this);
}


SceneNode::~SceneNode()
{
}

void SceneNode::attachChild(nodePtr child)
{
	child->parent = this;
	children.push_back(std::move(child));
}

SceneNode::nodePtr SceneNode::detachChild(const SceneNode& node)
{
	auto found = std::find_if(children.begin(), children.end(), [&node](nodePtr& ptr){return ptr.get() == &node; });
	assert(found != children.end());
	nodePtr result = std::move(*found);
	result->parent = nullptr;
	children.erase(found);
	return result;
}

void SceneNode::draw(sf::RenderTarget& target, sf::RenderStates states) const
{
	states.transform *= getTransform();
	drawCurrent(target, states);
	drawChildren(target, states);
}

void SceneNode::drawChildren(sf::RenderTarget& target, sf::RenderStates states) const
{
	for (auto &child : children)
	{
		child->draw(target, states);
	}
}

void SceneNode::updateChildren(sf::Time deltaTime)
{
	for (auto &child : children)
	{
		child->update(deltaTime);
	}
}

void SceneNode::update(sf::Time deltaTime)
{
	updateCurrent(deltaTime);
	updateChildren(deltaTime);
}

void SceneNode::updateCurrent(sf::Time deltaTime)
{

}

void SceneNode::drawCurrent(sf::RenderTarget& target, sf::RenderStates states) const
{

}



bool SceneNode::collision(const SceneNode& left, const SceneNode& right)
{
	//if (left.getTag() == "sprite" || right.getTag() == "sprite")
	//	return false;
	//if (left.getGlobalBounds().intersects(right.getGlobalBounds()))
	//	return true;

	if (abs(left.getPosition().x - right.getPosition().x) < 1.f)
	{
		if (abs(left.getPosition().y - right.getPosition().y) < 1.f)
			return true;
	}
	return false;
	
}

void SceneNode::getNodeCollisions(SceneNode& node, std::set<nodePair>& collSet)
{
	if (this != &node&&collision(*this, node))
		collSet.insert(std::minmax(this, &node));
	for (nodePtr& child:children)
	{
		child->getNodeCollisions(node, collSet);
	}
}

void SceneNode::sceneCollisions(SceneNode& scene, std::set<nodePair>& collSet)
{
	getNodeCollisions(scene, collSet);
	for (nodePtr& child : children)
	{
		child->sceneCollisions(*child, collSet);
	}
}
void SceneNode::nodeCollisions(std::set<nodePair>& collSet)
{
	SceneNode* tmp = this;
	while (tmp->parent)
		tmp = tmp->parent;
	tmp->getNodeCollisions(*this, collSet);
}
const std::string& SceneNode::getTag() const
{
	return tag;
}

sf::FloatRect SceneNode::getLocalBounds() const
{
	return sf::FloatRect();
}

sf::FloatRect SceneNode::getGlobalBounds() const
{
	return getTransform().transformRect(getLocalBounds());
}
