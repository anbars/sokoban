#ifndef MENU_ST_H
#define MENU_ST_H

#include "State.h"
#include "Container.h"
#include "Button.h"
#include <vector>

namespace states{


	class MenuState : public State
	{
	public:

	private:
		GUI::Container components;
		sf::Sprite titleSprite;
	public:
		MenuState(StateStack& stack, Context& context);
		~MenuState();
		virtual void draw() override;
		virtual bool update(sf::Time deltaTime) override;
		virtual bool handleEvent(const sf::Event& event) override;
	};

}

#endif

