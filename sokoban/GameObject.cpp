#include "GameObject.h"
#include "ResourceHolder.h"

GameObject::GameObject(const sf::Texture& texture, std::string tag) :sprite(texture), SceneNode(tag), moving(false), speed(200.f)
{
	tex::centerOrigin(sprite);
	targetPosition = getPosition();
}



void GameObject::drawCurrent(sf::RenderTarget& target, sf::RenderStates states) const
{
	target.draw(sprite, states);
}

bool GameObject::step(const sf::Vector2f &direction)
{
	return false;
}

void GameObject::updateCurrent(sf::Time deltaTime)
{
	sf::Vector2f pMove;
	int hDir = toMove.x / abs(toMove.x);
	int vDir = toMove.y / abs(toMove.y);
	pMove.x = std::min(toMove.x*hDir, speed*deltaTime.asSeconds())*hDir;
	pMove.y = std::min(toMove.y*vDir, speed*deltaTime.asSeconds())*vDir;
	toMove.x -= pMove.x;
	toMove.y -= pMove.y;
	move(pMove);
	if (!toMove.x&&!toMove.y&&moving)
	{
		moving = false;
		setPosition(targetPosition);
	}
}

bool GameObject::canStand()
{
	std::set<SceneNode::nodePair> colSet;
	nodeCollisions(colSet);
	for (const auto& col : colSet)
	{
		if (col.first != this)
		{
			if (col.first->getTag() != "sprite" && col.first->getTag() != "tagetpoint")
				return false;
		}
		else
		{
			if (col.second->getTag() != "sprite" && col.second->getTag() != "tagetpoint")
				return false;
		}
	}
	return true;
}

void GameObject::setTexture(const sf::Texture& texture)
{
	sprite.setTexture(texture);
}

sf::FloatRect GameObject::getLocalBounds() const
{
	sf::FloatRect bounds = sprite.getLocalBounds();
	bounds.width *= getScale().x;
	bounds.height *= getScale().y;
	return bounds;
}

bool GameObject::isMoving() const
{
	return moving;
}