#ifndef LEV_SET_H_
#define LEV_SET_H_

#include "Level.h"
#include <vector>
#include <memory>

class LevelSet
{
public:
	typedef std::shared_ptr<LevelSet> Ptr;
	typedef std::unique_ptr<Level> lvlPtr;
private:
	std::vector<Level::Map> set;
	sf::RenderWindow& window;
	TextureHolder& textures;
	SoundHolder& sounds;
	int currentLevel;
	std::string id;

	void loadFromTxt(std::string filename);
public:
	int size() const;
	LevelSet(sf::RenderWindow& window, TextureHolder& textures, SoundHolder& sounds, std::string filename);
	lvlPtr get(int index, Score& score);
	lvlPtr getCurrent(Score& score);
	void nextLevel();
	void setCurrent(int index);

	bool isFinished() const;

	std::string getID() const;
};

#endif