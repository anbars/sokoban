#ifndef COMMON_H_
#define COMMON_H_

#include <random>

namespace math{

	template <typename T>
	T RandomRange(T min, T max)
	{
		std::random_device rd;
		std::default_random_engine generator(rd());
		std::uniform_real_distribution<T> dist(min, max);
		return dist(generator);
	}
}

#endif