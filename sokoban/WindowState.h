#ifndef WINDOW_STATE_H_
#define WINDOW_STATE_H_

#include "State.h"
#include "Container.h"

namespace states{

	class WindowState abstract : public State
	{
		sf::RectangleShape shadowRect;

	protected:
		GUI::Container components;
		sf::Sprite backSprite;
	public:
		WindowState(StateStack& stack, Context& context);
		~WindowState();

		virtual void draw() override;
		virtual bool update(sf::Time deltaTime) override;
		virtual bool handleEvent(const sf::Event& event) override;
	};

}
#endif
