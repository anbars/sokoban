#include "Container.h"
#include <assert.h>

namespace GUI{

	Container::Container() : selectedChild(-1), children()
	{
	}


	Container::~Container()
	{
	}

	bool Container::isSelectable() const
	{
		return false;
	}

	bool Container::hasSelection() const
	{
		return selectedChild >= 0;
	}

	Container& Container::pack(Component::Ptr component)
	{
		children.push_back(component);
		if (!hasSelection() && component->isSelectable())
			select(children.size() - 1);
		return *this;
	}

	void Container::select(int index)
	{
		assert(index >= 0 && index <= children.size() - 1);
		if (children[index]->isSelectable())
		{
			if (hasSelection())
				children[selectedChild]->deselect();
			selectedChild = index;
			children[selectedChild]->select();
		}
	}

	void Container::selectNext()
	{
		if (!hasSelection())
			return;
		int index = selectedChild;
		do {
			index++;
			if (index > children.size() - 1)
				index = 0;
		} while (!children[index]->isSelectable());
		select(index);
	}

	void Container::selectPrev()
	{
		if (!hasSelection())
			return;
		int index = selectedChild;
		do{
			index--;
			if (index < 0)
				index = children.size() - 1;
		} while (!children[index]->isSelectable());
		select(index);
	}

	void Container::handleEvent(const sf::Event& event, const sf::RenderWindow& window)
	{
		if (hasSelection() && children[selectedChild]->isActive())
		{
			children[selectedChild]->handleEvent(event, window);
		}
		else if (event.type == sf::Event::KeyReleased)
		{
			if (event.key.code == sf::Keyboard::Up||event.key.code==sf::Keyboard::Left)
			{
				selectPrev();
			}
			if (event.key.code == sf::Keyboard::Down || event.key.code == sf::Keyboard::Right)
			{
				selectNext();
			}
			if (event.key.code == sf::Keyboard::Return)
			{
				if (hasSelection())
					children[selectedChild]->activate();
			}
		}
		else if (event.type == sf::Event::MouseMoved)
		{
			for (int i = 0; i < children.size();i++)
			{
				if (children[i]->isMouseHover(window))
					select(i);
			}
		}
		else
		{
			for (auto& child : children)
				child->handleEvent(event, window);
		}
	}

	void Container::draw(sf::RenderTarget& target, sf::RenderStates states) const
	{
		for (auto& child : children)
		{
			child->draw(target, states);
		}
	}

	sf::FloatRect Container::getLocalBounds() const
	{
		return sf::FloatRect();
	}

	sf::FloatRect Container::getGlobalBounds() const
	{
		return sf::FloatRect();
	}

	void Container::clear()
	{
		children.clear();
		selectedChild = -1;
	}
}