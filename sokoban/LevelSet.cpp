#include "LevelSet.h"
#include "ResourceHolder.h"
#include <fstream>
#include <exception>
#include "DataTables.h"
LevelSet::LevelSet(sf::RenderWindow& window, TextureHolder& textures, SoundHolder& sounds, std::string filename) :window(window), textures(textures), currentLevel(0), id("Undefined"), sounds(sounds)
{
	loadFromTxt(filename);
}

int LevelSet::size() const
{
	return set.size();
}

LevelSet::lvlPtr LevelSet::get(int index, Score& score)
{
	assert(index >= 0 && index <= (set.size() - 1));
	lvlPtr level(new Level(window,textures,sounds, set[index], score, std::make_pair(id, index)));
	currentLevel = index;
	return level;
}

void LevelSet::nextLevel()
{
	currentLevel++;
}

LevelSet::lvlPtr LevelSet::getCurrent(Score& score)
{
	return get(currentLevel, score);
}

void LevelSet::loadFromTxt(std::string filename)
{
	using tex::Textures;
	std::fstream file;
	file.open(filename);
	if (!file.is_open())
	{
		throw std::runtime_error("LevelSet::loadFromTxt->Failed to load file " + filename);
	}
	id = filename;
	auto levelTable = LevelTable::getInstance();
	Level::Map levelMap;
	std::string line;
	while (std::getline(file, line))
	{
		if (line.empty())
		{
			if (!levelMap.empty())
			{
				levelTable->newEntity(std::make_pair(id, set.size()));
				set.push_back(levelMap);
			}
			levelMap.clear();
			continue;
		}
		if (line[0] == 'L' || line[0] == ';')
		{
			continue;
		}
		std::vector<Textures> row;
		for (char c : line)
		{
			switch (c)
			{
			case '#':
				row.push_back(Textures::Wall);
				break;
			case '@':
				row.push_back(Textures::Player);
				break;
			case '$':
				row.push_back(Textures::Box);
				break;
			case '.':
				row.push_back(Textures::TargetPlace);
				break;
			case '*':
				row.push_back(Textures::BoxActive);
				break;
			case '+':
				row.push_back(Textures::TextureCount);//�������� �������� ��� ������ �� ���� ���. 
				break;
			default:
				row.push_back(Textures::Flour);
				break;
			}
		}
		levelMap.push_back(row);
	}
}

void LevelSet::setCurrent(int index)
{
	assert(index >= 0 && index <= (set.size() - 1));
	currentLevel = index;
}

bool LevelSet::isFinished() const
{
	return currentLevel == (set.size() - 1);
}

std::string LevelSet::getID() const
{
	return id;
}
