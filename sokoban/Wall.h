#ifndef WALL_H
#define WALL_H_
#include "GameObject.h"
#include "ResourceHolder.h"

class Wall :public GameObject
{
public:
	Wall(TextureHolder& textures);
	virtual void updateCurrent(sf::Time deltaTime) override;
};

#endif
