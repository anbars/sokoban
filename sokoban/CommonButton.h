#ifndef COMMON_BUTTON_H_
#define COMMON_BUTTON_H_

#include "Component.h"
#include "ResourceHolder.h"
#include <functional>

namespace GUI{

	class CommonButton abstract :public Component
	{

		std::function<void()> action;
		sf::Sound sound;
	
	protected:
		sf::Sprite sprite;

	public:
		CommonButton(const sf::Texture& btnTexture, const sf::SoundBuffer& clickSound);
		virtual ~CommonButton();

		virtual bool isSelectable() const override;

		virtual void select() override;
		virtual void deselect() override;

		virtual void activate() override;

		void setAction(std::function<void()> newAction);

		virtual void draw(sf::RenderTarget& target, sf::RenderStates states) const override;
		virtual void handleEvent(const sf::Event& event, const sf::RenderWindow& window) override;

		void setColor(const sf::Color& newColor);

		virtual sf::FloatRect getLocalBounds() const override;
		virtual sf::FloatRect getGlobalBounds() const override;
	};

}
#endif