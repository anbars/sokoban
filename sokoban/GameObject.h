#ifndef GAMEOBJ_H_
#define GAMEOBJ_H_

#include "SceneNode.h"

class GameObject :	public SceneNode
{
	virtual void drawCurrent(sf::RenderTarget& target, sf::RenderStates states) const override;
protected:
	sf::Sprite sprite;
	sf::Vector2f toMove;
	sf::Vector2f targetPosition;
	bool moving;
	float speed;
	virtual void updateCurrent(sf::Time deltaTime) override;
	void setTexture(const sf::Texture& texture);
public:
	GameObject(const sf::Texture& texture, std::string tag);
	virtual bool step(const sf::Vector2f& direction);
	bool canStand();
	bool isMoving() const;
	virtual sf::FloatRect getLocalBounds() const override;
};

#endif