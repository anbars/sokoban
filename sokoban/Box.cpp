#include "Box.h"


Box::Box(TextureHolder& textures) :GameObject(textures.get(tex::Textures::Box), "box"), textures(textures)
{
}


void Box::updateCurrent(sf::Time deltaTime)
{
	active = false;
	std::set<SceneNode::nodePair> nodeColls;
	nodeCollisions(nodeColls);

	for (auto& col : nodeColls)
	{
		if (col.first->getTag() == "tagetpoint" || col.second->getTag() == "tagetpoint")
		{
			active = true;
			break;
		}
	}
	
	if (active)
		setTexture(textures.get(tex::Textures::BoxActive));
	else
		setTexture(textures.get(tex::Textures::Box));

	GameObject::updateCurrent(deltaTime);
}

bool Box::step(const sf::Vector2f& direction)
{
	assert(direction.x >= -1 && direction.x <= 1);
	assert(direction.y >= -1 && direction.y <= 1);
	toMove = direction * 40.f*getScale().x;
	move(toMove);
	if (canStand())
	{
		moving = true;
		targetPosition = getPosition();
		move(-toMove);
		return true;
	}
	else
	{
		move(-toMove);
		toMove.x = toMove.y = 0;
		return false;
	}
}

bool Box::isActive() const
{
	return active;
}