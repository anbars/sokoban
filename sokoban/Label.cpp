#include "Label.h"

namespace GUI {

	Label::Label(const std::string label, const FontHolder& fonts) : text(label, fonts.get(font::Fonts::LabelFont))
	{
		tex::centerOrigin(text);
	}

	bool Label::isSelectable() const
	{
		return false;
	}

	Label::~Label()
	{
	}

	void Label::handleEvent(const sf::Event& event, const sf::RenderWindow& window)
	{

	}

	void Label::draw(sf::RenderTarget& target, sf::RenderStates states) const
	{
		states.transform *= getTransform();
		target.draw(text, states);
	}

	sf::FloatRect Label::getLocalBounds() const
	{
		return text.getLocalBounds();
	}

	sf::FloatRect Label::getGlobalBounds() const
	{
		return getTransform().transformRect(text.getGlobalBounds());
	}

	void Label::setFontSize(unsigned int size)
	{
		text.setCharacterSize(size);
	}

	void Label::setText(std::string newText)
	{
		text.setString(newText);
	}
	
	void Label::setColor(sf::Color color)
	{
		text.setColor(color);
	}
}
