#include "MusicPlayer.h"
#include <tuple>

namespace audio{

	MusicPlayer::MusicPlayer()
	{
		music.setLoop(true);
		music.setVolume(50.f);
	}


	MusicPlayer::~MusicPlayer()
	{
	}

	void MusicPlayer::addToPlaylist(MusicThemes id, std::string filepath)
	{
		playlist.insert(std::make_pair(id, filepath));
	}

	void MusicPlayer::setTrack(MusicThemes id)
	{
		music.openFromFile(playlist[id]);
	}

	void MusicPlayer::Stop()
	{
		music.stop();
	}

	void MusicPlayer::Play()
	{
		music.play();
	}

	void MusicPlayer::Pause()
	{
		music.pause();
	}
}