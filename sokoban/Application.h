#ifndef APP_H
#define APP_H_

#include "StateStack.h"
#include "LevelSet.h"
#include "MusicPlayer.h"

class Application
{
	TextureHolder textures;
	FontHolder fonts;
	SoundHolder sounds;
	audio::MusicPlayer musicPlayer;
	sf::RenderWindow mWindow;
	states::StateStack stateStack;

	void processEvents();
	void update(sf::Time deltaTime);
	void render();

	void loadTextures();
	void loadFonts();
	void loadSounds();
	void setMusic();
	void registerStates();
public:
	Application();
	~Application();

	void run();
};


#endif
