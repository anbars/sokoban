#include "MenuState.h"

namespace states{

	MenuState::MenuState(StateStack& stack, Context& context) : State(stack, context)
	{
		using namespace GUI;
		
		auto button = new Button(getContext().textures, getContext().sounds, getContext().fonts, "Play");
		button->setAction([this](){
			requestStackPush(states::ID::SetSelect);
		});
		button->setPosition(sf::Vector2f(button->getLocalBounds().width/2, context.window.getView().getSize().y-button->getLocalBounds().height/2));
		components.pack(Component::Ptr(button));

		button = new Button(getContext().textures, getContext().sounds, getContext().fonts, "Exit");
		button->setAction([this](){
			requestStackPush(states::ID::Exit);
		});
		button->setPosition(sf::Vector2f(context.window.getView().getSize().x - button->getLocalBounds().width / 2, context.window.getView().getSize().y - button->getLocalBounds().height / 2));
		components.pack(Component::Ptr(button));

		titleSprite.setTexture(context.textures.get(tex::Textures::titleBack));
		tex::centerOrigin(titleSprite);
		titleSprite.setPosition(context.window.getView().getSize() / 2.f);

		getContext().musicPlayer.setTrack(audio::MusicThemes::Menu);
		getContext().musicPlayer.Play();
	}


	MenuState::~MenuState()
	{
		getContext().musicPlayer.Stop();
	}


	void MenuState::draw()
	{
		getContext().window.draw(titleSprite);
		getContext().window.draw(components);
	}

	bool MenuState::update(sf::Time deltaTime)
	{
		return false;
	}

	bool MenuState::handleEvent(const sf::Event& event)
	{
		if (event.type == sf::Event::Closed)
		{
			requestStackPush(states::ID::Exit);
		}
		else if (event.type == sf::Event::KeyReleased&&event.key.code == sf::Keyboard::Escape)
		{
			requestStackPush(states::ID::Exit);
		}
		else
		{
			components.handleEvent(event, getContext().window);
		}

		return false;
	}
}